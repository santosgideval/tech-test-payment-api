using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;

namespace tech_test_payment_api.Repositories
{
    public class PurchaseRepositories : IPurchaseRepository
    {
        private readonly List<Sales> _sales;

        public PurchaseRepositories() => _sales = new List<Sales>();

        public void InsertSale(SalesRequest sales)
        {
            var sale = new Sales(sales);
            _sales.Add(sale);
        }

        public SalesResponse GetSaleById(int id)
        {
            var sale = _sales.FirstOrDefault(f => f.Id == id);
            return new SalesResponse(sale);
        }

        public void ChangeStatus(int id, Status status)
        {

            var sale = _sales.FirstOrDefault(f => f.Id == id);

            switch (Enum.GetName(sale.Status))
            {
                case "AguardandoPagamento": //Aguardando pagamento
                    if (Enum.Equals(status, "PagamentoAprovado") || Enum.Equals(status, "Cancelada"))
                    {
                        sale.Status = status;
                    }
                    else
                    {
                        throw new ArgumentException("O status não pode ser atualizado para esse valor");
                    }
                    break;
                case "PagamentoAprovado": //Pagamento aprovado
                    if (Enum.Equals(status, "EnviadoTransportadora") || Enum.Equals(status, "Cancelada"))
                    {
                        sale.Status = status;
                    }
                    else
                    {
                        throw new ArgumentException("O status não pode ser atualizado para esse valor");
                    }
                    break;
                case "EnviadoTransportadora": //Enviado a transportadora
                    if (Enum.Equals(status, "Cancelada"))
                    {
                        sale.Status = status;
                    }
                    else
                    {
                        throw new ArgumentException("O status não pode ser atualizado para esse valor");
                    }
                    break;
                default: //Erro
                    //throw new ArgumentException("O status não pode ser atualizado para esse valor");
                    break;
            }

            sale.Status = status;
        }


    }
}