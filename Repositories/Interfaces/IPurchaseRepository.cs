
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repositories.Interfaces
{
    public interface IPurchaseRepository
    {
        SalesResponse GetSaleById(int id);
        
        void InsertSale(SalesRequest sale);

        void ChangeStatus(int id, Status status);
    }
}