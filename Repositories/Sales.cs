namespace tech_test_payment_api.Models
{
    public class Sales
    {
        public int Id { get; set; }
        public Status Status { get; set; }
        public Seller Seller { get; set; }
        public List<Item> Items { get; set; }
        public DateTime Date { get; set; }
        public int SaleIdentifier { get; set; }

        public Sales(SalesRequest request)
        {
            Id = request.Id;
            Status = 0;
            Seller = request.Seller;
            Items = request.Items;
            Date = request.Date;
            SaleIdentifier = request.SaleIdentifier;

        }

        public Sales(){}
    }
}