
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Repositories;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api-docs")]
    public class SalesController : ControllerBase
    {
        private readonly IPurchaseRepository _repository;

        public SalesController(IPurchaseRepository repository) => _repository = repository;

        [HttpPost]
        public IActionResult Sale(SalesRequest sale)
        {
            if(sale.Items.Count == 0)
            {
                return BadRequest(new { Erro = "A venda precisa ter pelo menos 1 item" });
            }

            _repository.InsertSale(sale);

            return Ok(sale);
        }

        [HttpGet("{id}")]
        public IActionResult GetSale(int id)
        {
            return Ok(_repository.GetSaleById(id));
        }

        [HttpPatch("{id}/{status}")]
        public IActionResult Sales(int id, Status status)
        {
            _repository.ChangeStatus(id, status);
            return Ok();
        }
    }
}