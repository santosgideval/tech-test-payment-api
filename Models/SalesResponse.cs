namespace tech_test_payment_api.Models
{
    public class SalesResponse
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public Seller Seller { get; set; }
        public List<Item> Items { get; set; }
        public DateTime Date { get; set; }
        public int SaleIdentifier { get; set; }

        public SalesResponse(Sales sales)
        {
            Id = sales.Id;
            Status = Enum.GetName(sales.Status);
            Seller = sales.Seller;
            Items = sales.Items;
            Date = sales.Date;
            SaleIdentifier = sales.SaleIdentifier;
        }

        public SalesResponse(){}
    }
}