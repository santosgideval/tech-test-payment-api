namespace tech_test_payment_api.Models
{
    public class Item
    {
        public string Product { get; set; }
        public int Quantity { get; set; }
        public decimal Value { get; set; }
        public decimal Total 
        {
            get => Value * Quantity;
        }
    }
}