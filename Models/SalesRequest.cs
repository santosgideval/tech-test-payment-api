using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class SalesRequest
    {
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public List<Item> Items { get; set; }
        public DateTime Date { get; set;}
        public int SaleIdentifier { get; set; }
    }
}